<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class YoutubeHome extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'youtubehome';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Native Web';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;
        $this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);

        parent::__construct();

        $this->displayName = $this->l('YoutubeHome');
        $this->description = $this->l('This module can add a youtube video on your homepage. Promote your activity !');

        $this->confirmUninstall = $this->l('');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('YOUTUBEHOME_LINK_VIDEO', "L83ceYS2w3g");
        Configuration::updateValue('YOUTUBEHOME_TITLE', "YoutubeHome");
        Configuration::updateValue('YOUTUBEHOME_SUBTITLE', "-Bienvenue-");

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHome');
    }

    public function uninstall()
    {
        Configuration::deleteByName('YOUTUBEHOME_LINK_VIDEO');
        Configuration::deleteByName('YOUTUBEHOME_TITLE');
        Configuration::deleteByName('YOUTUBEHOME_SUBTITLE');

        $urlImage = _PS_MODULE_DIR_.'youtubehome/views/img/*';
        @unlink($urlImage.Configuration::get('YOUTUBEHOME_LINK_IMAGE'));

        Configuration::deleteByName('YOUTUBEHOME_LINK_IMAGE');
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitYoutubeHomeModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign(array(
            'moduleUrl' => Context::getContext()->shop->getBaseURL(true),
            'youtube_embeded' => "https://www.youtube.com/embed/",
            'youtubeLink' => Configuration::get('YOUTUBEHOME_LINK_VIDEO'),
            'youtubeImage' => Configuration::get('YOUTUBEHOME_LINK_IMAGE'),
            'youtubeTitle' => Configuration::get('YOUTUBEHOME_TITLE'),
            'youtubeSubtitle' => Configuration::get('YOUTUBEHOME_SUBTITLE'),

        ));

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/youtubehome.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitYoutubeHomeModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-youtube-play"></i>',
                        'desc' => $this->l('Enter your youtube end link'),
                        'name' => 'YOUTUBEHOME_LINK_VIDEO',
                        'label' => $this->l('Link'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-youtube-play"></i>',
                        'desc' => $this->l('Enter your youtube title'),
                        'name' => 'YOUTUBEHOME_TITLE',
                        'label' => $this->l('Title'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-youtube-play"></i>',
                        'desc' => $this->l('Enter your youtube subtitle'),
                        'name' => 'YOUTUBEHOME_SUBTITLE',
                        'label' => $this->l('Subtitle'),
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->l('Image'),
                        'name' => 'YOUTUBEHOME_LINK_IMAGE',
                        'size' => 30,
                        'required' => false
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        $values = array(
            'YOUTUBEHOME_LINK_VIDEO' => Configuration::get('YOUTUBEHOME_LINK_VIDEO'),
            'YOUTUBEHOME_LINK_IMAGE' => Configuration::get('YOUTUBEHOME_LINK_IMAGE'),
            'YOUTUBEHOME_TITLE' => Configuration::get('YOUTUBEHOME_TITLE'),
            'YOUTUBEHOME_SUBTITLE' => Configuration::get('YOUTUBEHOME_SUBTITLE')
        );
        return $values;

    }

    public function uploadImageUser($urlImage, $oldImage)
    {
        if (isset($_FILES['YOUTUBEHOME_LINK_IMAGE']['tmp_name']) && isset($_FILES['YOUTUBEHOME_LINK_IMAGE']['name']) && $_FILES['YOUTUBEHOME_LINK_IMAGE']['name']) {
            if ($_FILES['YOUTUBEHOME_LINK_IMAGE']['error'] == 0 && is_uploaded_file($_FILES['YOUTUBEHOME_LINK_IMAGE']['tmp_name'])) {
                $extension = Tools::strtolower(Tools::substr(strrchr($_FILES['YOUTUBEHOME_LINK_IMAGE']['name'], '.'), 1));
                $file_name = 'fond.'.$extension;
                $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');

                if (in_array($extension, array('jpg', 'gif', 'jpeg', 'png')) && $_FILES['YOUTUBEHOME_LINK_IMAGE']['size'] < 2000000) {
                    if ($error = ImageManager::validateUpload($_FILES['YOUTUBEHOME_LINK_IMAGE'])) {
                        $this->errors[] = $error;
                    } elseif (!$temp_name || !move_uploaded_file($_FILES['YOUTUBEHOME_LINK_IMAGE']['tmp_name'], $temp_name)) {
                        $this->errors[] = $this->l('Can not upload the file');
                    } elseif (!ImageManager::resize($temp_name, $urlImage.$file_name, null, null, $extension)) {
                        $this->errors[] = $this->l('An error occurred during the image upload process.');
                    }
                    if (isset($temp_name)) {
                        @unlink($temp_name);
                        @unlink($urlImage.'fileType');
                        if (isset($oldImage)) {
                            @unlink($urlImage.$oldImage);
                        }
                    }
                    return $file_name;
                } else {
                    $this->warnings[] = $this->l('An error occurred during image upload. Are you sure its jpg, gif or png file ? Or his size is < 2mo ?');
                }
            }
        } else {
            return $oldImage;
        }
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $urlImage = _PS_MODULE_DIR_.'youtubehome/views/img/';

        $oldImage= Configuration::get('YOUTUBEHOME_LINK_IMAGE');

        $values = array(
            'YOUTUBEHOME_LINK_VIDEO' => Tools::getValue('YOUTUBEHOME_LINK_VIDEO'),
            'YOUTUBEHOME_TITLE' => Tools::getValue('YOUTUBEHOME_TITLE'),
            'YOUTUBEHOME_SUBTITLE' => Tools::getValue('YOUTUBEHOME_SUBTITLE'),
            'YOUTUBEHOME_LINK_IMAGE' => $this->uploadImageUser($urlImage, $oldImage)
        );

        Configuration::updateValue('YOUTUBEHOME_LINK_VIDEO', $values['YOUTUBEHOME_LINK_VIDEO']);
        Configuration::updateValue('YOUTUBEHOME_TITLE', $values['YOUTUBEHOME_TITLE']);
        Configuration::updateValue('YOUTUBEHOME_SUBTITLE', $values['YOUTUBEHOME_SUBTITLE']);
        Configuration::updateValue('YOUTUBEHOME_LINK_IMAGE', $values['YOUTUBEHOME_LINK_IMAGE']);

        Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name);
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayHome()
    {
        $this->context->smarty->assign(array(
            'moduleUrl' => Context::getContext()->shop->getBaseURL(true),
            'youtube_embeded' => "https://www.youtube.com/embed/",
            'youtubeLink' => Configuration::get('YOUTUBEHOME_LINK_VIDEO'),
            'youtubeImage' => Configuration::get('YOUTUBEHOME_LINK_IMAGE'),
            'youtubeTitle' => Configuration::get('YOUTUBEHOME_TITLE'),
            'youtubeSubtitle' => Configuration::get('YOUTUBEHOME_SUBTITLE'),
        ));

        return $this->display(__FILE__, 'youtubehome.tpl');
    }
}
