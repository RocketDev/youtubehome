<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{youtubehome}prestashop>youtubehome_99629a9ed23e02f2c5f50a4c9f59917b'] = 'YoutubeHome';
$_MODULE['<{youtubehome}prestashop>youtubehome_cd0f70f4a9d5c519e8a6d7ae7f689a9a'] = 'Ce module peut ajouter une vidéo youtube sur la page d\'accueil. ';
$_MODULE['<{youtubehome}prestashop>youtubehome_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{youtubehome}prestashop>youtubehome_5b276ed017d3f9f85a6a772ed27a915f'] = 'Entrez la fin de votre lien youtube';
$_MODULE['<{youtubehome}prestashop>youtubehome_97e7c9a7d06eac006a28bf05467fcc8b'] = 'Lien';
$_MODULE['<{youtubehome}prestashop>youtubehome_be53a0541a6d36f6ecb879fa2c584b08'] = 'Image';
$_MODULE['<{youtubehome}prestashop>youtubehome_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{youtubehome}prestashop>youtubehome_dc75b70fb639a9b40637265045bebf79'] = 'Vous ne pouvez pas upload le fichier';
$_MODULE['<{youtubehome}prestashop>youtubehome_7cc92687130ea12abb80556681538001'] = 'Une erreur est survenue pendant le processus d\'upload';
$_MODULE['<{youtubehome}prestashop>youtubehome_1be359f05bb62c72f207e1b30ac896ba'] = 'Une erreur est survenue pendant l\'upload de l\'image. Êtes vous sûr d\'envoyer un fichier .jpg, .gif ou png ? Sa taille est elle bien inférieur à 2mo ?';
$_MODULE['<{youtubehome}prestashop>youtubehome_0abd1814de45aa463b1d7476f8728b72'] = 'Bienvenue sur YoutubeHome';
$_MODULE['<{youtubehome}prestashop>youtubehome_57a77f42fd709acf3aa500d17136fdbc'] = 'Merci d\'utiliser notre module';
$_MODULE['<{youtubehome}prestashop>youtubehome_180fb434f3798a2224dbe39b4599b3ad'] = 'Vous pouvez paramétrer votre vidéo youtube ici. Si une vidéo est défini, vous la verrez apparaitre ici';
$_MODULE['<{youtubehome}prestashop>youtubehome_bfbe28fc46b92b0b18a841ba1c2f3e40'] = 'Si vous souhaitez changer votre vidéo, copiez le code contenu dans la fin de votre lien dans l\'input puis sauvegardez, c\'est tout !';
$_MODULE['<{youtubehome}prestashop>youtubehome_d7fa38a4b7954b6e386b87812899827a'] = 'Aucune image configuré pour le moment';
