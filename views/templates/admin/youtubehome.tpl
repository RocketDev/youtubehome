{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<h3><i class="icon icon-youtube-play"></i> {l s='YoutubeHome' mod='youtubehome'}</h3>
	<p>
		<strong>{l s='Welcome to YoutubeHome !' mod='youtubehome'}</strong><br />
		{l s='Thanks to use our module.' mod='youtubehome'}<br />
		{l s='You can set you home youtube video here. If a video is set, you can see it below.' mod='youtubehome'}
        {l s='If you want to change your vidéo. Go copy your link and paste in the input. And save ! It\'s all !' mod='youtubehome'}

	</p>
	<br />
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<h3>Video</h3>
			<iframe src="{$youtube_embeded|escape:'htmlall':'UTF-8'}{$youtubeLink|escape:'htmlall':'UTF-8'}" id="youtubeAdminRender" frameborder="0" width="80%" height="300px"></iframe>
		</div>
		<div class="col-md-6 col-sm-12">
			<h3>Image</h3>
            {if $youtubeImage}
				<img width="50%" src="{$moduleUrl|escape:'htmlall':'UTF-8'}modules/youtubehome/views/img/{$youtubeImage|escape:'htmlall':'UTF-8'}" alt="backgroundImage">
				{else}
                {l s='No image set yet' mod='youtubehome'}
			{/if}
		</div>
	</div>
	<div class="row">
		<p>{l s='Title' mod='youtubehome'} : {$youtubeTitle}</p>
		<p>{l s='Subtitle' mod='youtubehome'} : {$youtubeSubtitle}</p>
	</div>
</div>

